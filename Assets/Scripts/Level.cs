using System;
using System.Collections.Generic;

public class Level
{
    private readonly List<Ingredient> _recipe;
    private int _nextIngredientIndex;

    public bool Passed => _nextIngredientIndex >= _recipe.Count;
    public Ingredient? NextIngredient => _nextIngredientIndex < _recipe.Count ? _recipe[_nextIngredientIndex] : null;
    public IReadOnlyCollection<Ingredient> Recipe => _recipe;

    private Level(IEnumerable<Ingredient> recipe)
    {
        _recipe = new List<Ingredient>(recipe);
    }

    public void SetNextIngredient()
    {
        if (Passed) return;
        ++_nextIngredientIndex;
    }

    public static Level Generate(List<Ingredient> ingredients, int recipeLength, Random rnd)
    {
        var recipe = new Ingredient[recipeLength];
        int ingredientsCount = ingredients.Count;
        
        for (var i = 0; i < recipeLength; ++i)
        {
            recipe[i] = ingredients[rnd.Next(0, ingredientsCount)];
        }

        recipe[^1] = Ingredient.Bun;

        return new Level(recipe);
    }
}