using System;
using TMPro;
using UnityEngine;

namespace DefaultNamespace
{
    public class TimerViwe : MonoBehaviour
    {
        [SerializeField] private GameManager gameManager;
        
        public TMP_Text view;

        private void Update()
        {
            view.text = $"{(int)gameManager._timer}";
        }
    }
}