using System;
using System.Collections.Generic;
using UnityEngine;

public class IngredientView : MonoBehaviour
{
    [Serializable]
    public struct IngredientGO
    {
        public Ingredient ingredient;
        public GameObject go;
    }

    [SerializeField] private List<IngredientGO> views = new();

    public RectTransform RectTransform => (RectTransform)transform;

    public void SetIngredient(Ingredient ingredient)
    {
        foreach (IngredientGO ingredientView in views)
        {
            ingredientView.go.SetActive(ingredientView.ingredient == ingredient);
        }
    }
}