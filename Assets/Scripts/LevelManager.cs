using System;
using System.Collections.Generic;
using GamePlayLoop;
using NaughtyAttributes;
using UnityEngine;
using Random = System.Random;

public class LevelManager : MonoBehaviour
{
    [SerializeField] private IngredientsPredicator ingredientsPredicator;

    private Level _currentLevel;
    private int _currentLevelNumber;
    private readonly Random _rnd = new();

    public event Action<IReadOnlyCollection<Ingredient>> RecipeUpdated;
    public event Action<Ingredient?> NextIngredientUpdated;
    public event Action LevelPassed;
    public IReadOnlyCollection<Ingredient> Recipe => _currentLevel.Recipe;
    public Ingredient? NextIngredient => _currentLevel.NextIngredient;
    
    [Button]
    public void IngredientCaught()
    {
        _currentLevel.SetNextIngredient();
        NextIngredientUpdated?.Invoke(NextIngredient);
        if (_currentLevel.Passed) LevelPassed?.Invoke();
    }

    public void NextLevel()
    {
        _currentLevelNumber += 1;
        _currentLevel = Level.Generate(
            ingredientsPredicator.Predicate(_currentLevelNumber),
            _currentLevelNumber,
            _rnd
        );
        RecipeUpdated?.Invoke(Recipe);
        NextIngredientUpdated?.Invoke(NextIngredient);
    }

    public void Reset()
    {
        _currentLevelNumber = 0;
        _currentLevel = null;
    }
}