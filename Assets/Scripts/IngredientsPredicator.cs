using System;
using System.Collections.Generic;
using UnityEngine;

public class IngredientsPredicator : MonoBehaviour
{
    [Serializable]
    public struct LevelIngredients
    {
        public int minLevel;
        public int maxLevel;
        public List<Ingredient> ingredients;

        public bool ContainsLevel(int levelNumber)
        {
            return levelNumber >= minLevel && levelNumber <= maxLevel;
        }
    }

    [SerializeField] private List<LevelIngredients> levelIngredients = new();
    
    public List<Ingredient> Predicate(int levelNumber)
    {
        foreach (LevelIngredients levelIngredient in levelIngredients)
        {
            if (levelIngredient.ContainsLevel(levelNumber)) return levelIngredient.ingredients;
        }
        
        return new List<Ingredient>
        {
            Ingredient.Cheese,
            Ingredient.Beef,
            Ingredient.Tomato
        };
    }
}