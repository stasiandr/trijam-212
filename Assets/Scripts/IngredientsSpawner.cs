using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GamePlayLoop;
using UnityEngine;

public class IngredientsSpawner : MonoBehaviour
{
    [SerializeField] private CanonManager canonManager;
    private Coroutine _spawnCoroutine;
    private List<Ingredient> _recipeList;

    public void StartSpawn(float delay)
    {
        _spawnCoroutine = StartCoroutine(SpawnRoutine(delay));
    }

    public void StopSpawn()
    {
        StopCoroutine(_spawnCoroutine);
        _spawnCoroutine = null;
    }

    public void UpdateSpawnList(IEnumerable<Ingredient> recipe)
    {
        _recipeList = recipe.ToList();
    }

    private IEnumerator SpawnRoutine(float delay)
    {
        var wait = new WaitForSeconds(delay);
        while (true)
        {
            canonManager.Shoot(_recipeList[Random.Range(0, _recipeList.Count)]);
            yield return wait;
        }
    }
}