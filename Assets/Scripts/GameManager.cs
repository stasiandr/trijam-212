using System;
using System.Collections;
using GamePlayLoop;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private LevelManager levelManager;
    [SerializeField] private IngredientsSpawner ingredientsSpawner;
    [SerializeField] private IngredientsCollector ingredientsCollector;
    [SerializeField] private float levelTime;
    [SerializeField] private float spawnDelay;
    [SerializeField] private GameObject endGameScreen;

    public bool playing;
    public float _timer;

    public void StartGame()
    {
        StartCoroutine(NextLevelRoutine());
    }

    public void Restart()
    {
        SceneManager.LoadScene(0);
    }

    public void StopGame()
    {
        endGameScreen.SetActive(true);
        ingredientsSpawner.StopSpawn();
        ingredientsCollector.isCollecting = false;
    }
    
    private void OnLevelPassed()
    {
        spawnDelay = Mathf.Clamp(1f / levelManager.Recipe.Count, 0.1f, 1000);
        levelTime = Mathf.Clamp(levelManager.Recipe.Count * 2, 20, 60);

        playing = false;
        ingredientsSpawner.StopSpawn();
        ingredientsCollector.isCollecting = false;
        ingredientsCollector.DropTower();
        StartCoroutine(NextLevelRoutine());
    }

    private void OnIngredientCaught()
    {
        levelManager.IngredientCaught();
        ingredientsCollector.PushMask(levelManager.NextIngredient);
    }

    private void Awake()
    {
        levelManager.LevelPassed += OnLevelPassed;
        ingredientsCollector.ingredientCaught += OnIngredientCaught;
        StartGame();
    }

    private void Update()
    {
        if (!playing) return;
        _timer -= Time.deltaTime;
        if (_timer <= 0) StopGame();
    }

    private void OnDestroy()
    {
        levelManager.LevelPassed += OnLevelPassed;
        ingredientsCollector.ingredientCaught += OnIngredientCaught;
    }

    private IEnumerator NextLevelRoutine()
    {
        yield return new WaitForSeconds(3);
        
        levelManager.NextLevel();
        ingredientsSpawner.UpdateSpawnList(levelManager.Recipe);
        ingredientsSpawner.StartSpawn(spawnDelay);
        ingredientsCollector.PushMask(levelManager.NextIngredient);
        ingredientsCollector.isCollecting = true;
        playing = true;
        _timer = levelTime;
    }
}