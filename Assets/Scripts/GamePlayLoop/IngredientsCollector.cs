using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using Random = UnityEngine.Random;

namespace GamePlayLoop
{
    public class IngredientsCollector : MonoBehaviour
    {
        [SerializeField] private Transform stackPoint;

        [SerializeField] private float itemsMargin;

        public Stack<IngredientMono> IngredientsStack;

        public event Action ingredientCaught;

        public bool isCollecting = true;

        public void PlaceOnStack(IngredientMono ingredientMono)
        {
            Destroy(ingredientMono.rb);
            Destroy(ingredientMono.GetComponent<DestroyTimer>());
            
            var pos = LocalPositionFromNumber(IngredientsStack.Count);
            
            ingredientMono.transform.SetParent(stackPoint);
            ingredientMono.transform.rotation = Quaternion.identity;
            ingredientMono.transform.localPosition = pos;

            transform.localPosition = pos;

            IngredientsStack.Push(ingredientMono);
        }

        public IngredientMono TakeItemFromStack()
        {
            var item = IngredientsStack.Pop();

            if (item.TryGetComponent<Rigidbody>(out _)) return null;

            item.AddComponent<Rigidbody>().AddForce(Random.onUnitSphere * 20f, ForceMode.VelocityChange);
            Destroy(item);
            item.AddComponent<DestroyTimer>().timer = 3;
            item.transform.parent = null;
            

            return item;
        }

        public void DropTower()
        {
            StartCoroutine(DropTowerAnimation());
        }

        private Ingredient? _mask;

        public void PushMask(Ingredient? ingredient)
        {
            _mask = ingredient;
        }

        private IEnumerator DropTowerAnimation()
        {
            while (IngredientsStack.Count != 0)
            {
                yield return new WaitForSeconds(0.1f);
                TakeItemFromStack();
            }
        }

        private Vector3 LocalPositionFromNumber(int count) => new(0, count * itemsMargin, 0);

        private void Awake()
        {
            IngredientsStack = new Stack<IngredientMono>();
        }


        public void OnTriggerEnter(Collider other)
        {
            if (!isCollecting) return;

            if (!other.attachedRigidbody.TryGetComponent<IngredientMono>(out var ingredient)) return;
            
            if (_mask != ingredient.self) return;

            PlaceOnStack(ingredient);
            
            ingredientCaught?.Invoke();
        }
    }
}