using System;
using System.Collections;
using UnityEngine;

namespace GamePlayLoop
{
    public class DestroyTimer : MonoBehaviour
    {
        public float timer;
        
        private IEnumerator Start()
        {
            while (timer > 0)
            {
                yield return null;
                timer -= Time.deltaTime;
            }
            
            Destroy(gameObject);
        }
    }
}