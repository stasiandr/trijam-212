using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float playerSpeed = 10;
    public float dashMultiplier = 1.5f;
    
    private Rigidbody _rb;

    public ParticleSystem burstPS;

    private void Awake()
    {
        _rb = GetComponent<Rigidbody>();
    }


    private void Update()
    {
        var y = Input.GetAxis("Vertical");
        var x = Input.GetAxis("Horizontal");
        var direction = new Vector3(x, 0, y);
        
        _rb.AddForce(direction * (Time.deltaTime * playerSpeed), ForceMode.Acceleration);


        if (Input.GetButtonDown("Jump"))
        {
            _rb.AddForce(direction * (playerSpeed * dashMultiplier), ForceMode.VelocityChange);
            burstPS.Stop();
            burstPS.Play();
        }
    }
}
