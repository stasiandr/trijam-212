using System;
using System.Collections;
using System.Linq;
using GamePlayLoop;
using UnityEngine;
using Random = UnityEngine.Random;

public class Canon : MonoBehaviour
{
    [SerializeField] private IngredientMono[] itemsToSpawn;
    [SerializeField] private Transform spawnPoint;

    [SerializeField] private float rotationSpeed = 1;
    [SerializeField] private float foodLMaxLifetime = 2;

    [SerializeField] private float maxAngle = 60;
    [SerializeField] private Vector2 minMaxForce = new(20, 100);  

    public IngredientMono GetByEnum(Ingredient value) => itemsToSpawn.First(i => i.self == value);

    // private IEnumerator Start()
    // {
    //     while (true)
    //     {
    //         yield return new WaitForSeconds(1);
    //         
    //         Shoot(Ingredient.Beef);
    //     }
    // }

    public void Shoot(Ingredient ingredient)
    {
        var direction = FindRandomVectorForDirection(spawnPoint.transform.forward, maxAngle);
        var force = Random.Range(minMaxForce.x, minMaxForce.y);
        
        Shoot(direction, force, ingredient);
    }

    private Vector3 FindRandomVectorForDirection(Vector3 target, float angle)
    {
        while (true)
        {
            var vec = Random.onUnitSphere;

            if (Vector3.Angle(vec, target) < angle) return vec;
        }
    }

    private void Shoot(Vector3 direction, float force, Ingredient ingredient)
    {
        var go = Instantiate(GetByEnum(ingredient), spawnPoint.transform.position, spawnPoint.transform.rotation);
        go.rb.AddForce(direction * force, ForceMode.Impulse);
        go.rb.AddTorque(Random.onUnitSphere * rotationSpeed);
    }
}