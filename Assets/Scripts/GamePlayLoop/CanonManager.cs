using UnityEngine;

namespace GamePlayLoop
{
    public class CanonManager : MonoBehaviour
    {
        public Canon[] canons;
        
        public void Shoot(Ingredient ingredient)
        {
            canons[Random.Range(0, canons.Length)].Shoot(ingredient);
        }
    }
}