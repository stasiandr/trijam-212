using System;
using Unity.Mathematics;
using UnityEngine;

namespace GamePlayLoop
{
    public class PlayerBody : MonoBehaviour
    {
        public Transform target;
     
        private Vector3 _oldTargetPosition;

        private void Update()
        {
            var direction = target.position - _oldTargetPosition;
            
            transform.position = target.position;
            

            if (direction != Vector3.zero)
            {
                transform.forward = direction;
            }


            _oldTargetPosition = target.position;
        }
    }
}