using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

[DefaultExecutionOrder(-100)]
public class RecipeView : MonoBehaviour
{
    [SerializeField] private LevelManager levelManager;
    [SerializeField] private IngredientView ingredientViewTemplate;

    private readonly List<IngredientView> _recipeView = new();

    private void OnRecipeUpdated(IReadOnlyCollection<Ingredient> recipe)
    {
        Debug.Log($"Recipe {string.Join(' ', recipe)}");
        foreach (IngredientView view in _recipeView)
        {
            Destroy(view.gameObject);
        }

        _recipeView.Clear();

        List<Ingredient> recipeList = recipe.ToList();
        recipeList.Reverse();
        
        float yPosition = ingredientViewTemplate.RectTransform.anchoredPosition.y * recipeList.Count;
        
        foreach (Ingredient ingredient in recipeList)
        {
            IngredientView view = Instantiate(ingredientViewTemplate, ingredientViewTemplate.transform.parent);
            view.SetIngredient(ingredient);
            _recipeView.Add(view);
            view.RectTransform.anchoredPosition = new Vector2(view.RectTransform.anchoredPosition.x, yPosition);
            yPosition += view.RectTransform.rect.height;
            view.gameObject.SetActive(true);
        }
    }

    private void OnNextIngredientUpdated(Ingredient? obj)
    {
        Debug.Log($"Next ingredient {obj}");
        foreach (IngredientView view in _recipeView)
        {
            view.RectTransform.anchoredPosition = new Vector2(view.RectTransform.anchoredPosition.x, view.RectTransform.anchoredPosition.y + view.RectTransform.rect.height);
        }
    }
    
    private void Awake()
    {
        levelManager.RecipeUpdated += OnRecipeUpdated;
        levelManager.NextIngredientUpdated += OnNextIngredientUpdated;
    }

    private void OnDestroy()
    {
        levelManager.RecipeUpdated -= OnRecipeUpdated;
        levelManager.NextIngredientUpdated -= OnNextIngredientUpdated;
    }
}