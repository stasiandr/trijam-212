public enum Ingredient
{
    None = 0,
    Beef = 1,
    Tomato = 2,
    Cheese = 3,
    Bun = 4,
}
