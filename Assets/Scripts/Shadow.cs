using System;
using System.Collections;
using UnityEngine;

public class Shadow : MonoBehaviour
{
    private IEnumerator Start()
    {
        var target = transform.parent;
        transform.parent = null;
        
        transform.rotation = Quaternion.identity;

        while (target != null)
        {
            var pos = target.transform.position;

            pos.y = 0.001f;

            transform.position = pos;

            yield return null;
        }

        Destroy(gameObject);
    }
}